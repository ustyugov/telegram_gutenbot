import re
import asyncio
import logging
import slixmpp as sx

from datetime import datetime as dt
from functools import partial
from slixmpp import exceptions as slixceptions

import tgwrapper.mapping as tg

import utils

log = logging.getLogger('guten_bot.mucbot')

nick_start_re = re.compile(r'^([^,:]*)')


# noinspection PyPropertyAccess,PyUnusedLocal
class MUCBot(sx.ClientXMPP):
    # MUC keepalive
    __KA_INTERVAL = 120
    __KA_TIMEOUT = 30
    __KA_MAX_RETRY = 5

    #
    # Event handlers
    #

    def _session_start(self, event):
        self.get_roster()

        if self.__send_initial_presence: self.send_presence(ptype='available')
        self.join_muc(self.nick)

        self.schedule('ka-ping-{}'.format(int(dt.now().timestamp())), self.__KA_INTERVAL, self.__ping_handler)

        p = self.tg.send_message(self.tg.chat_id, '`Connected`', tg.Message.ParseMode.markdown)

        log.info('Session start: {}'.format(p))

    def _disconnected(self, event):
        if self.__away_handle is not None: self.__away_handle.cancel()
        if self.__xa_handle is not None: self.__xa_handle.cancel()

        p = self.tg.send_message(self.tg.chat_id, '`Disconnected`', tg.Message.ParseMode.markdown)

        del self.occupants

        log.info('Disconnect: {}'.format(p))

    def __ping_handler(self, event=None):
        return asyncio.ensure_future(self._ping_muc(event))

    async def _ping_muc(self, event=None):
        # log.debug('MUC keepalive ping...')
        try:
            iq = self.make_iq_get(queryxmlns='http://jabber.org/protocol/disco#info',
                                  ifrom=self.__jid, ito=self.__mucjid)
            # resp = await iq.send(timeout=self.__timeout)
            await iq.send(timeout=self.__KA_TIMEOUT)

        except (slixceptions.IqError,
                slixceptions.IqTimeout,
                sx.xmlstream.xmlstream.NotConnectedError) as e:
            log.error('Error during request ping: {}'.format(e))
            if self.__retries < self.__KA_MAX_RETRY:
                self.__retries += 1
                self.reconnect()
            else:
                self.stop()

        else:
            # log.debug('MUC keepalive done: {}'.format(resp))
            self.__retries = 0
            self.schedule('ka-ping-{}'.format(int(dt.now().timestamp())),
                          self.__KA_INTERVAL * (self.__retries + 1),
                          self.__ping_handler)

    #
    # Message handlers
    #

    def __prepare_body(self, msg: sx.Message):
        body = msg['body']

        body = utils.html_esc(body)

        if msg['from'] == self.__mucjid:
            # return '<code>{}</code>'.format(body)
            return None

        pre = ''
        tag = 'b'
        delim = ':'
        nick = msg['mucnick']

        if msg['type'] in ['chat', 'normal'] and msg['from'].bare == self.__mucjid:
            pre = '<code>private</code>\n'
            nick = msg['from'].resource

        elif msg['type'] == 'groupchat':
            if body.startswith('/me '):
                body = body[4:]
                pre = '*'
                tag = 'i'
                delim = ' '

        else:
            return None

        return '{pre}<{tag}>{nick}</{tag}>{delim} {body}'.format(pre=pre, tag=tag, delim=delim, nick=nick, body=body)

    def _message(self, msg: sx.Message):
        if msg['type'] == 'error': return

        log.info('Got message: {}'.format(msg))

        if msg['mucnick'] == self.nick: return
        if msg['mucnick'] in self.__bots and msg['body'].startswith('Заголовок: '): return

        ignore = list(set(
            [o['nick'] for o in self.occupants if o.get('jid') in self.__ignore] +
            [rec for rec in self.__ignore if '@' not in rec]
        ))

        if msg['mucnick'] in ignore or \
           (nick_start_re.search(msg['body']).group(0) in ignore and self.nick not in msg['body']):
            return

        msgs = self.db.get_msgs(self.tg.chat_id, xmpp_from=msg['from'].full)
        mids = [m[4] for m in msgs]

        if not msg['id'] or msg['id'].startswith(self._id_prefix):
            ts = int(dt.now().timestamp())
            ts -= ts % 2

            msg['id'] = utils.md5dig(msg['body'] + str(ts))

        if msgs and (msg['id'] in mids or (msg.get('replace') and msg['replace']['id'] in mids)): return

        reply = True if self.nick in msg['body'] or msg['type'] in ['chat', 'normal'] else False

        body = self.__prepare_body(msg)

        if body is None:
            log.warning('Message not sent: {}'.format(msg))
            return

        elif len(body) > 4096:
            if reply and self.tg.last_msg:
                p = self.tg.send_message(self.tg.chat_id, body[:4096], tg.Message.ParseMode.html,
                                         reply_to_message_id=self.tg.last_msg, reply_markup=tg.ForceReply())
            else:
                p = self.tg.send_message(self.tg.chat_id, body[:4096], tg.Message.ParseMode.html)

            body = body[4096:]
            while len(body) > 4096:
                self.tg.send_message(self.tg.chat_id, body[:4096], tg.Message.ParseMode.html)
                body = body[4096:]

        elif reply and self.tg.last_msg:
            p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.html,
                                     reply_to_message_id=self.tg.last_msg,
                                     reply_markup=tg.ForceReply() if msg['mucnick'] not in self.__bots else None)
        else:
            p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.html)

        if p is not None:
            self.db.add_msg(self.tg.chat_id, p.message_id, msg['from'].full, msg['to'].bare, msg['id'])
            log.info('TG Message sent: {}'.format(p))
        else:
            log.warning('TG Message not sent')

    def _message_correction(self, msg: sx.Message):
        if msg['type'] == 'error': return

        log.info('Edited message: {}'.format(msg))

        msgs = self.db.get_msgs(self.tg.chat_id, xmpp_from=msg['from'].full)

        if not msgs or not msg.get('replace') or msg['replace']['id'] not in [m[4] for m in msgs]: return

        if msg['mucnick'] == self.nick: return

        ignore = list(set(
            [o['nick'] for o in self.occupants if o.get('jid') in self.__ignore] +
            [rec for rec in self.__ignore if '@' not in rec]
        ))

        if msg['mucnick'] in ignore or \
           (nick_start_re.search(msg['body']).group(0) in ignore and self.nick not in msg['body']):
            return

        body = self.__prepare_body(msg)

        if body is None:
            log.warning('Message not edited: {}'.format(msg))
            return
        elif len(body) > 4094:
            body = body[:4093] + '…'

        body += ' ✓'

        msgs = [m for m in msgs if m[4] == msg['replace']['id']]
        message_id = msgs[-1][1] if msgs[-1] else None

        if message_id is None:
            log.error('Message not edited: {}'.format(msg))
            return

        p = self.tg.edit_message_text(body, chat_id=self.tg.chat_id,
                                      message_id=message_id,
                                      parse_mode=tg.Message.ParseMode.html)

        if p is not None:
            self.db.upd_msg(self.tg.chat_id, p.message_id, msg['from'].full, msg['to'].bare, msg['id'])
            log.info('TG Message edited: {}'.format(p))
        else:
            log.warning('TG Message not edited')

    def _message_error(self, msg: sx.Message):
        log.error('Error message: {}'.format(msg))

        body = '<code>error</code>\n{}'.format(msg['body'])

        p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.html)

        log.info('TG message error: {}'.format(p))

    #
    # Groupchat subject change
    #

    def _groupchat_subject(self, msg: sx.Message):
        if msg['type'] == 'error': return

        log.info('MUC Subject: {}'.format(msg))

        body = msg['subject']

        if not body:
            log.warning('Message not sent: {}'.format(msg))
            return

        msgs = self.db.get_msgs(self.tg.chat_id, xmpp_from=msg['from'].bare)
        mids = [m[4] for m in msgs]

        if not msg['id'] or msg['id'].startswith(self._id_prefix):
            msg['id'] = 'subject-' + utils.md5dig(body)

        if msgs and msg['id'] in mids: return

        body = utils.html_esc(body)

        if msg['from'] == self.__mucjid:
            body = '<code>Subject:</code>\n{}'.format(body)
        else:
            body = '<b>{}</b> <code>changed subject to:</code>\n{}'.format(msg['mucnick'], body)

        if len(body) > 4096:
            p = self.tg.send_message(self.tg.chat_id, body[:4096],
                                     tg.Message.ParseMode.html, disable_web_page_preview=True)
            body = body[4096:]
            while len(body) > 4096:
                self.tg.send_message(self.tg.chat_id, body[:4096],
                                     tg.Message.ParseMode.html, disable_web_page_preview=True)
                body = body[4096:]
        else:
            p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.html, disable_web_page_preview=True)

        if p is not None:
            self.db.del_msgs(self.tg.chat_id, msg['from'].bare)
            self.db.add_msg(self.tg.chat_id, p.message_id, msg['from'].bare, msg['to'].bare, msg['id'])
            log.info('TG Message sent: {}'.format(p))
        else:
            log.warn('TG Message not sent')

    #
    # Presence handlers
    #

    def _presence(self, presence: sx.Presence):
        if presence['from'].bare != self.__mucjid.bare:
            log.warn('Not a groupchat presence: {}'.format(presence))
            return

        if presence['type'] == 'error': self._presence_error(presence)
        elif presence['type'] == 'unavailable': self._presence_unavailable(presence)
        elif presence['type'] in ['available', 'chat', 'dnd', 'away', 'xa']: self._presence_available(presence)
        else: log.warning('Unknown presence: {}'.format(presence))

    def _presence_available(self, presence: sx.Presence):
        log.info('Available presence {}'.format(presence))

        nick = presence['muc']['nick']
        affiliation = presence['muc']['affiliation']
        role = presence['muc']['role']
        status = presence['type']

        jid = presence['muc'].get('jid')

        self.db.del_msgs(self.tg.chat_id, presence['from'].full)

        for o in self.occupants:
            if o['nick'] == nick:
                o['status'] = status
                o['affiliation'] = affiliation
                o['role'] = role

                if jid: o['jid'] = jid.bare

                return

        o = {
            'nick': nick,
            'affiliation': affiliation,
            'role': role,
            'status': status
        }
        if jid: o['jid'] = jid.bare

        self.occupants.append(o)

        if nick == self.nick:
            if self.__send_initial_presence: self.refresh_status(5, 15)
        else:
            if self.__show_join_leave:
                if jid: body = '<code>{} join ({})</code>'.format(nick, jid)
                else: body = '<code>{} join</code>'.format(nick)
                p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.html)

    def _presence_unavailable(self, presence: sx.Presence):
        log.info('Unavailable presence {}'.format(presence))

        nick = presence['muc']['nick']

        status_codes = presence['muc'].xml.findall('{http://jabber.org/protocol/muc#user}status')
        status_codes = [int(code.get('code')) for code in status_codes]

        nick_new = presence['muc'].get_xml_item().get('nick')

        status = presence['status']

        body = None

        for i in range(len(self.occupants)):
            if self.occupants[i]['nick'] == nick:
                self.occupants.pop(i)

                if nick == self.nick:
                    if self.__away_handle is not None: self.__away_handle.cancel()
                    if self.__xa_handle is not None: self.__xa_handle.cancel()

                    body = 'offline'

                    for code in status_codes:
                        body = 'kicked' if code == 307 else \
                               'banned' if code == 301 else \
                               'nickname changed' if code == 303 else \
                               body

                        if code == 303 and nick_new: self.__nick = nick_new

                        if code in [301, 303]:
                            break
                        elif code != 110:
                            self.reconnect()

                    body = '<code>{}</code>\n<code>{}</code>'.format(body, utils.html_esc(status))
                else:
                    if self.__show_join_leave: body = '<code>{} leave room</code>'.format(nick)
                break

        if body:
            p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.html)
            if p: log.info('TG Unavailable: {}'.format(p))
            else: log.info('TG Unavailable')

    def _presence_error(self, presence: sx.Presence):
        log.error('Error presence {}'.format(presence))

        body = None

        if presence['error']['code'] in ['400', '409']:
            if presence['error']['code'] == '400':
                body = '`Invalid nickname`'
            elif presence['error']['code'] == '409':
                body = '`That nickname is registered by another person`'

            self.join_muc(self.nick)

        elif presence['error']['code'] == '404':
            body = '`Can not connect`'

        if body:
            p = self.tg.send_message(self.tg.chat_id, body, tg.Message.ParseMode.markdown)
            log.info('TG message error: {}'.format(p))

    #
    # MUC messages routines
    #

    def send_muc(self, message_id, body, edit=False):
        msgs = self.db.get_msgs(self.tg.chat_id, tg_msg_id=message_id)

        if msgs and not edit: return

        message = self.make_message(self.__mucjid.full, body, mtype='groupchat')
        if edit: message['replace']['id'] = msgs[-1][4]

        message.send()

        if not edit: self.db.add_msg(self.tg.chat_id, message_id, self.__jid.bare, self.__mucjid.full, message['id'])
        else: self.db.upd_msg(self.tg.chat_id, message_id, self.__jid.bare, self.__mucjid.full, message['id'])

        log.info('XMPP Message {}: {}'.format('edited' if edit else 'sent', message))

    #
    # Private muc chat routines
    #

    def send_private(self, nick, message_id, body, edit=False):
        msgs = self.db.get_msgs(self.tg.chat_id, tg_msg_id=message_id)

        if msgs and not edit: return

        p_jid = '{}/{}'.format(self.__mucjid.full, nick)

        message = self.make_message(p_jid, body, mtype='chat')
        if edit: message['replace']['id'] = msgs[-1][4]

        message.send()

        if not edit: self.db.add_msg(self.tg.chat_id, message_id, self.__jid.bare, p_jid, message['id'])
        else: self.db.upd_msg(self.tg.chat_id, message_id, self.__jid.bare, self.__mucjid.full, message['id'])

        log.info('XMPP private message {}: {}'.format('edited' if edit else 'sent', message))

    #
    # Some other utils
    #

    def status_change(self, nick, show=None, ptype='available'):
        self.send_presence(pshow=show, ptype=ptype)
        self.send_presence(pshow=show, ptype=ptype, pto='{}/{}'.format(self.__mucjid, nick))

    def join_muc(self, nick):
        self.plugin['xep_0045'].join_muc(self.__mucjid, nick, wait=True)

    def refresh_status(self, away_timeout=15, xa_timeout=60):
        for u in self.occupants:
            if u['nick'] == self.nick and u['status'] != 'available':
                self.status_change(self.nick)
                break

        if self.__away_handle is not None: self.__away_handle.cancel()
        if self.__xa_handle is not None: self.__xa_handle.cancel()

        self.__away_handle = self.loop.call_later(away_timeout * 60, partial(self.status_change, self.nick, 'away'))
        self.__xa_handle = self.loop.call_later(xa_timeout * 60, partial(self.status_change, self.nick, 'xa'))

    def toggle_statuses(self):
        self.__show_join_leave = not self.__show_join_leave
        return self.__show_join_leave

    #
    # Stream control routines
    #

    def start(self):
        if not self.tg:
            log.error('TG Bot not added')
            return None

        self.__connected_event = asyncio.Event()
        self.add_event_handler('session_start', lambda _: self.__connected_event.set())
        self.connect()

        return self.__connected_event

    def stop(self, app=None):
        self.__retries = 0

        p = self.tg.send_message(self.tg.chat_id, '`Disconnected`', tg.Message.ParseMode.markdown)
        log.debug('Disconnect: {}'.format(p))

        self.disconnect()

    #
    # properties
    #

    tg = property(lambda self: self.__tg)
    db = property(lambda self: self.__db)
    nick = property(lambda self: self.__nick)
    occupants = property(lambda self: self.__occupants)

    @tg.setter
    def tg(self, tg_):
        self.__tg = tg_ if self.__tg is None else self.__tg

    @occupants.deleter
    def occupants(self):
        self.__occupants = []

    #
    # Initialization
    #

    def __init__(self, db, jid, password, muc, nick, ignore, bots, send_initial_presence = None, show_join_leave = None):
        super().__init__(jid, password)

        self.__jid = sx.JID(jid)
        self.__mucjid = sx.JID(muc)
        self.__nick = nick

        self.__ignore = ignore
        self.__bots = bots

        if send_initial_presence != None:
            self.__send_initial_presence = send_initial_presence
        else:
            self.__send_initial_presence = True

        if show_join_leave != None:
            self.__show_join_leave = show_join_leave
        else:
            self.__show_join_leave = False

        self.__tg = None
        self.__db = db

        self.__occupants = []

        self.__connected_event = None

        self.__retries = 0

        # Autostatus
        self.__away_handle = None
        self.__xa_handle = None

        # register plugins
        self.register_plugin('xep_0030')  # Service Discovery
        self.register_plugin('xep_0004')  # Data Forms
        self.register_plugin('xep_0060')  # PubSub
        self.register_plugin('xep_0199')  # XMPP Ping
        self.register_plugin('xep_0045')  # MUCs
        # self.register_plugin('xep_0092')  # Version
        self.register_plugin('xep_0308')  # Edit messages

        # # setup version plugin
        # version = 'Unknown'
        # try:
        #     with open('VERSION') as vf:
        #         version = vf.read().strip()
        #         float(version)
        # except (IOError, ValueError):
        #     log.warn('Error parsing version file')

        # self.plugin['xep_0092'].config['software_name'] = 'GutenBot'
        # self.plugin['xep_0092'].config['version'] = version
        # self.plugin['xep_0092'].config['os'] = utils.dist()

        # add handlers
        self.add_event_handler('session_start', self._session_start)
        self.add_event_handler('session_end', self._disconnected)

        self.add_event_handler('message', self._message)
        self.add_event_handler('message_correction', self._message_correction)
        self.add_event_handler('message_error', self._message_error)

        self.add_event_handler('presence', self._presence)
        self.add_event_handler('groupchat_presence', self._presence)

        self.add_event_handler('groupchat_subject', self._groupchat_subject)
