aiohttp~=1.1.6
gunicorn~=19.6.0
requests~=2.12.3
slixmpp~=1.2.1
ujson~=1.35
git+https://bitbucket.org/gudvinr/tgwrapper.git@0.2
git+https://bitbucket.org/gudvinr/sqlitewrapper.git@0.1

