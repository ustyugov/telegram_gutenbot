import re
import logging
import hashlib
import platform
import unicodedata as ud


class StripFormatter(logging.Formatter):

    def format(self, record):
        s = super().format(record)
        return ' '.join(s.split())


def dist():
    pl = platform.platform().split('-')

    for i in range(len(pl)):
        if pl[i] == 'with': return " ".join([str(p).capitalize() for p in pl[i + 1:]])


def html_esc(text: str) -> str:
    return text.replace('&', '&amp;').replace('>', '&gt;').replace('<', '&lt;')


def md5dig(string: str) -> str:
    return hashlib.md5(string.encode()).hexdigest()


def exc_parse(exc_info):
    line = None
    fname = None
    name = None
    desc = None

    try:
        name = exc_info[0].__name__
        desc = exc_info[1]
        frame = exc_info[2]

        while frame.tb_next:
            frame = frame.tb_next

        line = frame.tb_lineno
        fname = frame.tb_frame.f_code.co_filename
    except:
        pass

    return name, desc, fname, line


def natsort(string: str) -> list:
    return [int(t) if t.isdigit() else ud.normalize('NFKD', t.lower()) for t in re.split('(\d+)', string)]
