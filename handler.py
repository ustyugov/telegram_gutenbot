from aiohttp import web
from datetime import datetime as dt
from urllib.parse import quote

import ujson
import os
import sys
import requests
import logging

from tgwrapper import Bot, Message, Update, ForceReply, MessageEntity
import utils

log = logging.getLogger('guten_bot.handler')


class ClientBot(Bot):

    #
    # Handle commands
    #

    def __process_cmd(self, msg, cmd, is_edit):

        if cmd == '/inmuc':
            return self.send_message(msg.from_.id, ClientBot.occupants(self.mb.occupants), Message.ParseMode.html)

        elif cmd == '/status':
            return self.send_message(msg.from_.id, '`Running`', Message.ParseMode.markdown)

        elif cmd == '/pm':
            return self.__process_pm(msg, is_edit)

        elif cmd == '/me':
            return self.__process_me(msg, is_edit)

        elif cmd == '/nick':
            return self.__process_nick(msg)

        elif cmd == '/preview':
            return self.__toggle_preview(msg.from_)

        elif cmd == '/toggle_statuses':
            return self.__toggle_statuses(msg.from_)

        elif cmd == '/start':
            return self.send_message(msg.chat.id, '`Greetings`', Message.ParseMode.markdown)

    #
    # Handle text messages
    #

    def __process_text(self, msg, body=None, is_edit=False):

        if msg.entities:
            for e in msg.entities:
                if e.type == MessageEntity.Type.bot_command and e.offset == 0:
                    return self.__process_cmd(msg, e.text, is_edit)

        if body is not None:
            msg.text = body

        return self.__process_msg(msg, is_edit)

    #
    # Handle files
    #

    def __process_document(self, msg, file_id, file_name=None):
        url = self.get_file_url(file_id)
        log.info('File URL: {}'.format(url))

        if not url:
            log.warning('Big file or not found')
            return self.send_message(msg.from_.id, '`Error sending file`', Message.ParseMode.markdown)

        if file_name:
            if len(file_name.split('.')) == 1:
                file_name += '.' + '.'.join(url.rsplit('/', 1)[1].rsplit('.', 2)[1:])
        else:
            file_name = url.rsplit('/', 1)[1]

        file_name = '{}_{}'.format(int(dt.now().timestamp()), file_name)

        path = os.getcwd() + os.sep + 'files'

        if not os.path.exists(path):
            os.mkdir(path)
        elif not os.path.isdir:
            os.remove(path)
            os.mkdir(path)

        path += os.sep + file_name

        try:
            data = requests.get(url, timeout=10).content

            with open(path, 'wb') as f:
                f.write(data)
        except IOError as e:
            log.error('Error handle doc: {}'.format(e.args))
            return self.send_message(msg.from_.id, '`Error sending file`', Message.ParseMode.markdown)

        msg.text = self.__uploader_url + quote(file_name) + ('\n{}'.format(msg.caption) if msg.caption else '')

        self.__process_msg(msg)

        body = '<code>link</code>\n{}'.format(self.__uploader_url + quote(file_name))

        return self.send_message(msg.chat.id, body, Message.ParseMode.html, True)

    #
    # Utils
    #

    def __process_nick(self, msg):
        p_len = len('/nick ')

        if len(msg.text) < p_len + 1:
            return self.send_message(msg.from_.id, '`nick`\nEnter your nickname to change nickname',
                                     Message.ParseMode.markdown,
                                     reply_markup=ForceReply())

        nick = msg.text[p_len:]

        if nick in [o['nick'] for o in self.mb.occupants]:
            return self.send_message(msg.from_.id, 'This username is already taken',
                                     Message.ParseMode.markdown)

        self.mb.join_muc(nick)

        return None

    def __process_me(self, msg, is_edit):
        p_len = len('/me ')

        if len(msg.text) < p_len + 1:
            reply = msg.reply_to_message

            if reply:
                for e in reply.entities:
                    if e.type == MessageEntity.Type.code and e.text == 'private':
                        return self.send_message(msg.from_.id, 'Send `/me <text>` to send this in private mode',
                                                 Message.ParseMode.markdown)

            return self.send_message(msg.from_.id, '`me`\nEnter message to send',
                                     Message.ParseMode.markdown,
                                     reply_markup=ForceReply())

        else:
            return self.__process_msg(msg, is_edit)

    def __process_pm(self, msg, is_edit):
        p_len = len('/pm ')

        body = msg.text[p_len:]
        reply = msg.reply_to_message

        if reply is not None and reply.from_.id != self.__chat_id:

            for e in reply.entities:
                if e.type == MessageEntity.Type.bold or e.type == MessageEntity.Type.italic:
                    nick = e.text

                    self.mb.send_private(nick, msg.message_id, body, is_edit)
                    return None
                elif e.type == MessageEntity.Type.code:
                    if e.text == 'me':
                        body = '/me ' + body
                    if e.text == 'pm':
                        break

        nick = body

        if not nick:
            return self.send_message(msg.from_.id, '`pm`\nEnter nick of recipient',
                                     Message.ParseMode.markdown, reply_markup=ForceReply())

        if nick not in [o['nick'] for o in self.mb.occupants]:
            text = '<code>No such user</code>'
            mkp = None
        else:
            text = '<code>private</code>\n<b>{}</b>'.format(utils.html_esc(nick))
            mkp = ForceReply()

        return self.send_message(msg.from_.id, text, Message.ParseMode.html, reply_markup=mkp)

    def __process_msg(self, msg, is_edit=False):
        body = msg.text
        reply = msg.reply_to_message

        private = False

        if reply is not None and reply.from_.id != self.__chat_id:
            nick = None

            for e in reply.entities:
                if e.type == MessageEntity.Type.code:
                    if e.text == 'private':
                        private = True
                        continue
                    elif e.text == 'me':
                        body = '/me ' + body
                        continue
                    elif e.text == 'nick':
                        msg.text = '/nick ' + body
                        return self.__process_nick(msg)
                    elif e.text == 'pm':
                        msg.text = '/pm ' + body
                        return self.__process_pm(msg, is_edit)

                elif e.type == MessageEntity.Type.bold or e.type == MessageEntity.Type.italic:
                    nick = e.text
                    break

            if nick is None:
                if not private:
                    self.mb.send_muc(msg.message_id, body, is_edit)

            elif private:
                self.mb.send_private(nick, msg.message_id, body, is_edit)

            else:
                self.mb.send_muc(msg.message_id, '{}, {}'.format(nick, body), is_edit)

        else:
            self.mb.send_muc(msg.message_id, body, is_edit)

        if not private:
            self.mb.refresh_status()
            self.__last_msg = msg.message_id

        return None

    def __toggle_preview(self, user):
        self.__preview_enabled = not self.preview_enabled

        text = '`Link preview is {}`'.format('enabled' if self.preview_enabled else 'disabled')
        return self.send_message(user.id, text, parse_mode=Message.ParseMode.markdown)

    def __toggle_statuses(self, user):
        text = '`Show join/leave statuses is {}`'.format('enabled' if self.mb.toggle_statuses() else 'disabled')
        return self.send_message(user.id, text, parse_mode=Message.ParseMode.markdown)

    def send_message(self, *args, **kwargs):
        if not self.preview_enabled \
                and 'disable_web_page_preview' not in kwargs.keys() \
                and (len(args) > 3 and not args[3]):

            kwargs['disable_web_page_preview'] = not self.preview_enabled

        if 'reply_to_message_id' not in kwargs.keys() \
                and 'reply_markup' not in kwargs.keys() \
                and (len(args) < 7 or (not args[5] and not args[6])):

            kwargs['disable_notification'] = True

        return super().send_message(*args, **kwargs)

    #
    # Pretty-print occupants list
    #

    @staticmethod
    def ocp_print(ocp) -> str:
        return '<b>{}</b>'.format(utils.html_esc(ocp['nick'])) + \
            (' ({})'.format(ocp['status']) if ocp['status'] != 'available' else '') + \
            (' - {}'.format(ocp['jid']) if ocp.get('jid') else '')

    @staticmethod
    def occupants(array):
        array.sort(key=lambda item: utils.natsort(item.get('nick')))

        members = [o for o in array if o['affiliation'] == 'member']
        admins = [o for o in array if o['affiliation'] in ['owner', 'admin']]
        others = [o for o in array if o['affiliation'] not in ['member', 'owner', 'admin']]

        if len(admins) > 0:
            text = 'Admins [{}/{}]\n'.format(len(admins), len(array))
            text += '\n'.join([ClientBot.ocp_print(o) for o in admins])
            text += '\n\n'

        if len(members) > 0:
            text += 'Members [{}/{}]\n'.format(len(members), len(array))
            text += '\n'.join([ClientBot.ocp_print(o) for o in members])
            text += '\n\n'

        if len(others) > 0:
            text += 'Other [{}/{}]\n'.format(len(others), len(array))
            text += '\n'.join([ClientBot.ocp_print(o) for o in others])

        return text.strip()

    #
    # requests handler
    #

    async def handle(self, request):
        r = await request.text()

        try:
            p = None
            is_edit = False

            update = Update(ujson.loads(r))

            log.info('TG Update object: {}'.format(update))

            if update.message is not None:
                msg = update.message
            elif update.edited_message is not None:
                msg = update.edited_message
                is_edit = True
            else:
                return web.Response(status=200)

            chat = msg.chat

            if chat.id != self.__chat_id:
                p = self.send_message(chat.id, '`Access Error`', Message.ParseMode.markdown)

            elif msg.text is not None:
                p = self.__process_text(msg, is_edit=is_edit)

            elif not is_edit:

                if msg.sticker is not None:
                    p = self.__process_text(msg, body=msg.sticker.emoji)

                elif msg.document is not None:
                    p = self.__process_document(msg, msg.document.file_id, msg.document.file_name)

                elif msg.photo is not None:
                    p = self.__process_document(msg, msg.photo[-1].file_id)

                elif msg.audio is not None:
                    doc = msg.audio
                    p = self.__process_document(msg, doc.file_id, '{} - {}'.format(doc.performer, doc.title))

                elif msg.video is not None:
                    p = self.__process_document(msg, msg.video.file_id)

                elif msg.voice is not None:
                    p = self.__process_document(msg, msg.voice.file_id)

            if p is not None:
                log.info('TG Responce: {}'.format(p))

        except:
            log.error('{}: {} in <{}:{}>'.format(
                *utils.exc_parse(sys.exc_info()))
            )
            self.mb.reconnect()
            raise
        finally:
            return web.Response(status=200)

    #
    # Properties
    #

    chat_id = property(lambda self: self.__chat_id)
    last_msg = property(lambda self: self.__last_msg)
    preview_enabled = property(lambda self: self.__preview_enabled)

    mb = property(lambda self: self.__mb)

    @mb.setter
    def mb(self, mb):
        self.__mb = mb

    def __init__(self, mucbot, token, chat_id, wh_url, uploader_url):
        super().__init__(token)
        self.__mb = mucbot

        self.__chat_id = chat_id

        self.__last_msg = None

        self.__preview_enabled = True

        self.__uploader_url = uploader_url

        if not self.get_webhook_info().url:
            self.set_webhook(wh_url)
