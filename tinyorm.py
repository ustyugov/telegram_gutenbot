import logging

from sqlitewrapper import Worker


log = logging.getLogger("guten_bot.tinyorm")


class TableMapper(object):
    __MSG_TABLE = "messages"

    #
    # Message management
    #

    def get_msgs(self, tg_chat_id, tg_msg_id=None, xmpp_from=None, xmpp_to=None, xmpp_msg_id=None):

        filters = {'tg_chat_id': tg_chat_id}

        if tg_msg_id is not None:
            filters['tg_msg_id'] = tg_msg_id
        if xmpp_from is not None:
            filters['xmpp_from'] = xmpp_from
        if xmpp_to is not None:
            filters['xmpp_to'] = xmpp_to
        if xmpp_msg_id is not None:
            filters['xmpp_msg_id'] = xmpp_msg_id

        return self.db.select_all(self.__MSG_TABLE, **filters)

    def del_msgs(self, tg_chat_id, jid):
        return self.db.query("""
            DELETE FROM {table}
            WHERE tg_chat_id=?
            AND (xmpp_from=? OR xmpp_to=?)
        """.format(table=self.__MSG_TABLE), tg_chat_id, jid, jid)

    def add_msg(self, tg_chat_id, tg_msg_id, xmpp_from, xmpp_to, xmpp_msg_id):
        return self.db.insert(self.__MSG_TABLE, tg_chat_id, tg_msg_id, xmpp_from, xmpp_to, xmpp_msg_id)

    def upd_msg(self, tg_chat_id, tg_msg_id, xmpp_from, xmpp_to, xmpp_msg_id):
        tg_upd = self.db.select_one(self.__MSG_TABLE, tg_chat_id=tg_chat_id, tg_msg_id=tg_msg_id,
                                    xmpp_from=xmpp_from, xmpp_to=xmpp_to)

        xmpp_upd = self.db.select_one(self.__MSG_TABLE, tg_chat_id=tg_chat_id, xmpp_from=xmpp_from,
                                      xmpp_to=xmpp_to, xmpp_msg_id=xmpp_msg_id)

        if xmpp_upd is None and tg_upd is not None:
            return self.db.update(self.__MSG_TABLE, ('xmpp_msg_id', xmpp_msg_id),
                                  tg_chat_id=tg_chat_id, tg_msg_id=tg_msg_id, xmpp_from=xmpp_from, xmpp_to=xmpp_to)
        elif xmpp_upd is not None and tg_upd is None:
            return self.db.update(self.__MSG_TABLE, ('tg_msg_id', tg_msg_id),
                                  tg_chat_id=tg_chat_id, xmpp_from=xmpp_from, xmpp_to=xmpp_to, xmpp_msg_id=xmpp_msg_id)
        else:
            return None

    def flush(self, limit=100):
        return self.db.query("""
            DELETE FROM {table} WHERE xmpp_msg_id NOT IN (
                SELECT xmpp_msg_id FROM {table}
                LIMIT {limit} OFFSET (SELECT COUNT(*) FROM {table})-{limit}
            ) AND xmpp_msg_id NOT LIKE 'subject-%'
        """.format(table=self.__MSG_TABLE, limit=limit))

    #
    # Service routines
    #

    def __create_msgs_table(self):
        self.db.create_table(self.__MSG_TABLE,
                             ('tg_chat_id', Worker.Column.INTEGER),
                             ('tg_msg_id', Worker.Column.INTEGER),
                             ('xmpp_from', Worker.Column.TEXT),
                             ('xmpp_to', Worker.Column.TEXT),
                             ('xmpp_msg_id', Worker.Column.TEXT))

    #
    # Properties
    #

    db = property(lambda self: self.__db)

    def __init__(self, sqlworker: Worker):
        self.__db = sqlworker

        if not self.__db.check_table(self.__MSG_TABLE):
            self.__create_msgs_table()
