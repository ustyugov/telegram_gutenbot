# GutenBot #

Реализация интерфейса [XMPP MUC](http://xmpp.org/extensions/xep-0045.html) в виде бота для [Telegram](https://telegram.org/).

### Зависимости ###

* Python 3.5 (используется async/await синтаксис)
* [aiohttp](https://aiohttp.readthedocs.io) - простой веб-сервер на asyncio
* [slixmpp](https://slixmpp.readthedocs.io/) - форк SleekXMPP, использующий asyncio
* [ujson](https://github.com/esnme/ultrajson) - быстрая библиотека де-/сериализации JSON
* [gunicorn](https://github.com/benoitc/gunicorn) (опционально) - wsgi сервер

### Описание ###

При работе есть несколько нюансов.

В виду ограничений на число ответов от бота эта платформа на данный момент не является подходящей для конференций с большим числом активных участников.
Тем не менее, при использовании одного бота для одного участника превышения лимитов не возникает.

Загрузка файлов осуществляется путём простого копирования файлов в публичную папку веб-сервера на том же физическом сервере, поэтому необходим внешний адрес.

### Настройка ###

* Установить зависимости в окружение или глобально с помощью `pip install -r requirements.txt`
* Создать `config.json` со своими настройками
* Создать нового бота с помощью [@BotFather](https://telegram.me/BotFather)
* Отправьте боту команду `/start`

### Формат файла конфигурации ###

```json
{
    "telegram": {
        // Адрес для установки вебхука
        "wh_url": "https://your.webhook.server.tld/path/<e.g. token>",
        // Токен, полученный от @BotFather
        "token": "0987654321:bhKJHFVHGJcfc-GJHVsad",
        // Ваш ID пользователя, с которым будет общаться бот
        "chat_id": 12345678,
        "uploader_url": "https://s.domain.tld/path/"
    },
    // Конфигурация XMPP
    "xmpp": {
        "jid": "login@server.tld",
        "password": "your_pass",
        "muc": "muc@adress.server.tld",
        "nick": "your_nick",
        // Ники чатботов
        "bots": ["IsidaBot", "BlackSmith"],
        // Список ников для игнора, так же игнорирует обращения от других пользователей(!).
        // Например: "SpamUser, цп в лс"
        "ignore": ["SpamUser", "ExGF"],
        // Слать ли пресенс "available" при запуске. Дефолтно true
        "send_initial_presence": true,
        // Показывать ли сообщения о входе/выходе. Дефолтно false
        "show_join_leave": false

    },
    "log": {
        "file": "guten_bot.log",
        // Отключить/включить подробный лог
        "debug": true,
        "verbose": false
    },
    "db": "guten_bot.db"
}
```