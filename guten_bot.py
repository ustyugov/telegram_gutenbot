import os

import logging
from logging.handlers import RotatingFileHandler

import asyncio

from aiohttp import web
import sqlitewrapper as sql

from mucbot import MUCBot
from tinyorm import TableMapper
from handler import ClientBot
from utils import StripFormatter

try:
    import ujson as json
except ImportError:
    import json

#
# Load config
#

config_file = os.getenv('CONFIG', "config.json")

with open(config_file) as fp:
    config = json.load(fp)

#
# Logging setup
#

logging.getLogger("requests.packages.urllib3").setLevel(logging.WARNING)
logging.getLogger("slixmpp").setLevel(logging.WARNING)
logging.getLogger("slixmpp.basexmpp").setLevel(logging.ERROR)

log_handler = RotatingFileHandler(config['log']['file'], maxBytes=10 * 1024 * 1024, backupCount=1)
log_handler.setFormatter(StripFormatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))

if config['log']['debug']:
    if config['log']['verbose']:
        level = logging.DEBUG
    else:
        level = logging.INFO
else:
    level = logging.WARNING

logging.basicConfig(level=level, handlers=[log_handler])


log = logging.getLogger("guten_bot")

#
# DB stuff
#

db = TableMapper(sql.Worker(config['db']))

#
# Setup handlers
#

mucbot = MUCBot(db, **config['xmpp'])

cbot = ClientBot(mucbot, **config['telegram'])
mucbot.tg = cbot


#
# Aiohttp stuff
#

conn_event = mucbot.start()

if not conn_event:
    log.error("XMPP can't start")
    exit(1)

loop = asyncio.get_event_loop()

app = web.Application(loop=loop)
app.router.add_route('POST', '/', cbot.handle)
app.on_cleanup.append(mucbot.stop)

loop.run_until_complete(conn_event.wait())

if __name__ == '__main__':
    web.run_app(app)
